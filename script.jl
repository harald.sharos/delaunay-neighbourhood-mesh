
# LEO:
dL = 550e3 # m, distance sender to receiver
pL = 50 # Million NOK, price per satellite
# iL = 90 # degree inclination
GTL = -17.0 # dB/K, gain of transmitter
PL = 97 # minutes, period of satellite

# HEO:
dH = 48000e3 # m, distance sender to receiver
pH = 5000 # Million NOK, price per satellite
# iH = 0 # degree inclination
GTH = 0.0 # dB/K, gain of transmitter

# Ground unit
f = 8.0e9 # Hz, frequency of ground unit
sz = 12 # bytes, size of message
# Access protocol random access
Rs = 100 # bit/s, burst rate
FEC = 1.0/4.0 # Forward error correction
EbN0_min = 10 # dB, threshold for receiver
Ptx = 10 # W, power of transmitter
Gtx = 10.0 # dBi, gain of transmitter, isotropic

# Carrier-to-noise-density ratio
c = 3e8 # m/s, speed of light
kb_db = -23*10*log10(1.38) # dB/K, Boltzmann constant

function CN0(EIRP, d, f, GT)
    ## EIRP in dBW
    ## d in m
    ## f in Hz
    ## GT in dB/K
    return EIRP + GT - 20*log10(4*pi*d*f/c) - kb_db
end

function EIRP(Ptx, Gtx)
    ## Ptx in W
    ## Gtx in dBi
    return 10.0*log10(Ptx) + Gtx
end

function EbN0(CN0, r = 1.0/4.0, Rs = 100.0)
    ## All argument units in dB

    EcN0 = CN0 - Rs
    return EcN0 - log10(r)
end

## Require EbN0 larger than 1dB, optimal 3dB for margin
CN0_H = CN0(EIRP(Ptx, Gtx), dH, f, GTH)
EbN0H = EbN0(CN0_H, FEC, Rs)
display(EbN0H)







# Factors in satellite transmission
## Transmission rate from ground sender
## Sender amplitude
## Distance between sender and receiver
## Number of satellites and availability
## Message latency
